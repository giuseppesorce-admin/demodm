/*
 * Copyright (C) 2014 Antonio Leiva Gordillo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.warpmobile.emuservertest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import it.warpmobile.emuservertest.R;
import com.squareup.picasso.Picasso;


public class DetailActivity extends ActionBarActivity {

    public static final String EXTRA_IMAGE = "DetailActivity:image";
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private String urlImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        }
        setActionBarIcon(R.drawable.ic_ab_drawer);

        setTitle("Demo");
        ImageView image = (ImageView) findViewById(R.id.image);
       ViewCompat.setTransitionName(image, EXTRA_IMAGE);

       if(getIntent().getExtras() !=null){
           urlImage= getIntent().getStringExtra(DetailActivity.EXTRA_IMAGE);
       }
        if(urlImage.length() >0){
            Picasso.with(getApplicationContext()).load(urlImage).into(image);
        }
    }

    protected void setActionBarIcon(int iconRes) {
        toolbar.setNavigationIcon(iconRes);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    public static void launch(MainActivity activity, View transitionView, String url) {
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, transitionView, DetailActivity.EXTRA_IMAGE);
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_IMAGE, url);

        Log.e("life", "URL_ "+url);

        if(transitionView !=null && url !=null && url.length() > 0) {

            ActivityCompat.startActivity(activity, intent, options.toBundle());
        }
    }
}
